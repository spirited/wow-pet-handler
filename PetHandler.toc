## Interface: 70300
## Title: Pet Slot Handler
## Notes: Displays a reminder when no pet slots are available to tame additional beasts.
## Author: Gareth Jensen
## Version: 1.0

PetHandler.lua